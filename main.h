/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet :  2                                                           *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Adresse IP                                                      *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 :  Driss Habi                                                  *
*                                                                             *
*  Nom-prénom2 :  Louis Leleu                                                 *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  main.h                                                   *
*                                                                             *
******************************************************************************/

int checkFormat(char adresseIP[]);
void  extraireEtConvertir(char adresseIP[], int ip[5]);
char* classeIP(int ip[5]);
char* publicOuPrive(int ip[5]);
void calculReseau(int ip[5], int ipReseau[5]);
void affichage(int valide, int ip[5], char classe[2], char usage[9], int ipReseau[5]);


