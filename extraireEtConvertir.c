/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet :  2                                                           *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Adresse IP                                                      *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 :  Driss Habi                                                  *
*                                                                             *
*  Nom-prénom2 :  Louis Leleu                                                 *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  extraireEtConvertir.c                                    *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include "main.h"
#include <stdlib.h>


void extraireEtConvertir(char adresseIP[], int ip[5]) {
	char adresse2[strlen(adresseIP)+1];
	strcpy(adresse2, adresseIP);
	char *ptr = strtok(adresseIP, ".");
	int i = 0;
	while(ptr != NULL) {
		int val = atoi(ptr);
		if(val < 0 || val > 255) {
			break;
		}
		ip[i] = val;
		ptr = strtok(NULL, i < 3 ? "." : "/");
		i++;
	}

	char *ptrM = strtok(adresse2, "/");
	i = 0;
	while(ptrM != 0) {
		if(i == 1) {
			int val = atoi(ptrM);
			ip[4] = val;
		}
		ptrM = strtok(NULL, "/");
		i++;
	}
}
