/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet :  2                                                           *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Adresse IP                                                      *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 :  Driss Habi                                                  *
*                                                                             *
*  Nom-prénom2 :  Louis Leleu                                                 *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  calculReseauHote.c                                                   *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"

int binToInt (char* input) {
    int i= strlen(input);
    int result = 0;
    for( int j=0; j<i; j++ ) {
        result <<= 1;
        if(input[j] == '1')
        {
            result |= 1;
        }  
	}
	return result;
}

void calculReseau(int ip[5], int ipReseau[5]) {
	int masque[4];
	int ipBuffer[32] = {0}; 
	char octet[8];
	
	for (int i = 0; i < ip[4]; i++) {
		ipBuffer[i] = 1; //remplir le buffer avec la notation CIDN ex: 11111111111111111111111111111100
	}
	int compt = 0;
	for (int i = 0; i < 4; i++) {
		for (int y = 0; y < 8; y++) {
			octet[y] = ipBuffer[compt] + '0';
			compt++;
		}
		masque[i] = binToInt(octet);
	}
	for (int i = 0; i < 4; i++) {
		ipReseau[i] = ip[i] & masque[i]; // oprérateur AND entre le masque et l'ip
	}
}


