GCC = gcc
BINAIRES = $(PATSUBST %.C %.O ${sources})
SOURCE = $(wildcard *.c)

all: main

%.o: %.c %.h
	${GCC} -c $<

main: main.c checkFormat.c extraireEtConvertir.c decoderIP.c calculReseauHote.c affichage.c
	${GCC} $^ -o $@

clean:
	rm main
	rm *.o
