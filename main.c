/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet :  2                                                           *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Adresse IP                                                      *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 :  Driss Habi                                                  *
*                                                                             *
*  Nom-prénom2 :  Louis Leleu                                                 *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  main.c                                                   *
*                                                                             *
******************************************************************************/


#include <stdio.h>
#include <string.h>
#include "main.h"

void main() {
	char adresseIP[19];
	printf("Saisissez une adresse IP : ");
	scanf("%s", adresseIP);
	int ip[5];
	char classe[2];
	char usage[9];
	int valide;
	int ipReseau[5];


	valide = checkFormat(adresseIP);
	if (valide == 1) {
		extraireEtConvertir(adresseIP, ip);
		strcpy (classe, classeIP(ip));
		strcpy (usage, publicOuPrive(ip));
		calculReseau(ip, ipReseau);
	}
	affichage(valide, ip, classe, usage, ipReseau);
	
}
