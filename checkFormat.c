/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet :  2                                                           *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Adresse IP                                                      *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 :  Driss Habi                                                  *
*                                                                             *
*  Nom-prénom2 :  Louis Leleu                                                 *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  checkFormat.c                                            *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <regex.h>
#include <stdlib.h>
#include "main.h"

int checkFormat(char *adresseIP)
{
    char* pattern = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\\/(3[0-2]|[1-2][0-9]|[0-9]))$";
	regex_t re;
	if (regcomp(&re, pattern, REG_EXTENDED|REG_NOSUB) != 0) { //vérifier si le regex se compile
		return 0;
	}
	int status = regexec(&re, adresseIP, 0, NULL, 0);
	regfree(&re);
	if (status != 0) { //le regex retourne faux.
		return 0;
	}
	return 1;
}

