/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet :  2                                                           *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Adresse IP                                                      *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 :  Driss Habi                                                  *
*                                                                             *
*  Nom-prénom2 :  Louis Leleu                                                 *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  affichage.c                                              *
*                                                                             *
******************************************************************************/

#include <stdio.h>

void affichage(int valide, int ip[5], char classe[2], char usage[9], int ipReseau[5]) {
	if (valide == 1) {
		printf("Adresse IP valide\n");
		printf("Adresse : %d.%d.%d.%d/%d\n", ip[0], ip[1], ip[2], ip[3], ip[4]);
		printf("Classe : %s\n", classe);
		printf("Usage : %s\n", usage);
		printf("IP Réseau : ");
		for (int i = 0; i < 4; i++) {
			printf(" %d ", ipReseau[i]);
		}
		printf("\n");
	} else {
		printf("Adresse IP non valide\n");
	}
}