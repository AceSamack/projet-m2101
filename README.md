Binôme 1 : Driss Habi
Binôme 2 : Louis Leleu

Notre application a pour but d'analyser une adresse IP fournie par l'utilisateur.
L'application renvoie ensuite :
- La classe de l'adresse
- Son usage publique/privé
- Son adresse réseau

Documentation :
CheckFormat 
Paramètre : adresse IP sous la forme d'une chaine de caractères
Renvoie 1 ou 0 en fonction de la correspondance.

extraireEtConvertir
Paramètre : adresse IP sous la forme d'une chaine de caractères, un tableau de 5 entiers.
Modifie le tableau afin d'y intégrer les 4 valeurs de l'IP ainsi que le masque.
échoue si l'une des valeur de l'ip est inférieur à 0 ou supérieur à 255.

classeIP
Paramètre : tableau contenant les valeurs de l'adresse IP.
Renvoie une chaîne de caractères (A, B, C, D ou E) en fonction de l'adresse IP fournie.

publicOuPrive
Paramètre : tableau contenant les valeurs de l'adresse IP.
Renvoie une chaîne de caractères ("publique" ou "privé") en fonction de l'adresse IP fournie.

BintoInt
Paramètre : Une chaîne de caractères.
Renvoie un entier.
permet de transformer un nombre binaire en entier.

calculReseau
Paramètre : Tableau adresse IP, Tableau contenant l'adresse réseau
Modifie le tableau adresse réseau afin d'y inclure les valeurs de l'adresse réseau de l'adresse IP fournie.